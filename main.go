package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

// var textArea *canvas.Text
var textArea *widget.TextGrid
var inputArea *widget.Entry

const WINDOW_TITLE = "BasicUI"

func main() {
	a := app.New()
	w := a.NewWindow(WINDOW_TITLE)

	//textArea = canvas.NewText("", color.White)
	textArea = widget.NewTextGrid()

	inputArea = widget.NewEntry()
	inputArea.SetPlaceHolder("Enter text...")
	label := widget.NewLabel("b1")

	hbox := container.NewHBox(label, inputArea, b1())
	hbox.Resize(fyne.NewSize(120, 80))

	content := container.NewVBox(hbox, b2(), layout.NewSpacer(), layout.NewSpacer(), textArea)

	w.SetContent(content)
	w.Resize(fyne.NewSize(80, 120))
	w.ShowAndRun()
}

func b1() *widget.Button {
	btn := widget.NewButton("b1", func() {
		fmt.Println("b1 clicked")
		val := inputArea.Text
		textArea.SetText("b1 clicked: " + val)
	})
	return btn
}

func b2() *widget.Button {
	btn := widget.NewButton("b2", func() {
		fmt.Println("b2 clicked")
		textArea.SetText("b2 clicked")
	})
	return btn
}
func run(cmd string) {
	a := strings.Split(cmd, " ")
	err := exec.Command(a[0], a[1:]...).Run()
	check(err)
}

func check(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
